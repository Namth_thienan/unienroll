import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './pages/main/main.component';
import { BlankComponent } from './views/blank/blank.component';
import { LoginComponent } from './pages/login/login.component';
import { ProfileComponent } from './views/profile/profile.component';
import { RegisterComponent } from './pages/register/register.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { AuthGuard } from './utils/guards/auth.guard';
import { NonAuthGuard } from './utils/guards/non-auth.guard';
import { TestComponent } from './components/test/test.component';
import { CreateStudentsComponent } from './components/create-students/create-students.component';
import { ApprovedComponent } from './components/approved/approved.component';
import { DistributeClassComponent } from './components/distribute-class/distribute-class.component';
import { FeeComponent } from './components/fee/fee.component';
import { ListClassComponent } from './components/list-class/list-class.component';
import { ReceivePapersComponent } from './components/receive-papers/receive-papers.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      {
        path: 'test',
        component: TestComponent,
      },

      {
        path: 'CreateSudents',
        component: CreateStudentsComponent,
      },
      {
        path: 'Approved',
        component: ApprovedComponent,
      },
      {
        path: 'Distribute',
        component: DistributeClassComponent,
      },
      {
        path: 'Fee',
        component: FeeComponent,
      },
      {
        path: 'ListClass',
        component: ListClassComponent,
      },
      {
        path: 'ReceivePapers',
        component: ReceivePapersComponent,
      },
      {
        path: '',
        component: DashboardComponent,
      },
    ],
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [NonAuthGuard],
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [NonAuthGuard],
  },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes), BrowserModule, HttpClientModule],
  exports: [RouterModule],
})
export class AppRoutingModule {}
