import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DistributeClassComponent } from './distribute-class.component';

describe('DistributeClassComponent', () => {
  let component: DistributeClassComponent;
  let fixture: ComponentFixture<DistributeClassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DistributeClassComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistributeClassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
