import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceivePapersComponent } from './receive-papers.component';

describe('ReceivePapersComponent', () => {
  let component: ReceivePapersComponent;
  let fixture: ComponentFixture<ReceivePapersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReceivePapersComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceivePapersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
